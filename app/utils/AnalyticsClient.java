package utils;

/* Copyright (c) 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.AccountEntry;
import com.google.gdata.data.analytics.AccountFeed;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.data.analytics.Dimension;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import exceptions.IGAnalyticsException;


/**
 * Google Data API's Java client library to access Google Analytics data.
 * 
 */
public class AnalyticsClient {

	/* Constants */
	public static final String ACCOUNTS_URL = "https://www.google.com/analytics/feeds/accounts/default";
	
	public static final String DATA_URL = "https://www.google.com/analytics/feeds/data";
	
	public static final String APP_NAME = "IGAnalytics";
	
	
	private AnalyticsService myService;

	public AnalyticsClient(String username, String password) {
		myService = new AnalyticsService(APP_NAME);
		
		try {
    		// Authenticate using ClientLogin
			myService.setUserCredentials(username, password);
			
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
	}

	public AnalyticsClient() {}

	

	/**
	 * Returns a data feed containing the accounts that the user logged in to
	 * the given AnalyticsService has access to.
	 * 
	 * @return A JSONArray containing an entry for each profile the logged-in
	 *         user has access to
	 * @throws IOException
	 *             If an error occurs while trying to communicate with the
	 *             Analytics server
	 * @throws ServiceException
	 *             If the API cannot fulfill the user request for any reason
	 */
	public JSONArray getAvailableAccounts()
			throws ServiceException, IOException {
		
		// Print a list of all accessible accounts
		URL feedUrl = new URL(ACCOUNTS_URL);
		AccountFeed accountFeed = this.myService.getFeed(feedUrl, AccountFeed.class);
		
		JSONObject jsonObject = null;
		JSONArray jsonArray = new JSONArray();
		

		for (AccountEntry entry : accountFeed.getEntries()) {
			jsonObject = new JSONObject();
			jsonObject.element("title",entry.getTitle().getPlainText());
			jsonObject.element("id",entry.getTableId().getValue());
			
			jsonArray.add(jsonObject);
		}
		
		return jsonArray;
	}

	public JSONObject getDataFeeds(String tableId, String dimensions, 
			String metrics, String startDate, String endDate, int maxResults,
			String sort, String filters) throws IGAnalyticsException{
		DataQuery query = null;
		JSONObject jsonObject = null;

		try {
			query = new DataQuery(new URL(DATA_URL));
			query.setIds(tableId);
			query.setStartDate(startDate);
			query.setEndDate(endDate);
			query.setDimensions(dimensions);
			query.setSort(sort);
			query.setMetrics(metrics);
			query.setFilters(filters);
			query.setMaxResults(maxResults);
			
			DataFeed data = myService.getFeed(query, DataFeed.class);
			JSONArray jsonArray = new JSONArray();
			
			if(data != null){
				String[] metricsArray = metrics.split(",");
				String[] dimsArray = null;
				if(dimensions != null && !dimensions.equals("")){
					dimsArray = dimensions.split(",");
				}
				JSONObject jsonEntry = null;
				for (DataEntry entry : data.getEntries()) {
					jsonEntry = new JSONObject();
					
					if(dimsArray != null){
						for (int i = 0; i < dimsArray.length; i++) {
							String value = entry.stringValueOf(dimsArray[i]);

							if(AnalyticsUtils.isParsable(Integer.class, value)){
								jsonEntry.element(dimsArray[i],Integer.parseInt(value));
								continue;
							}
							
							if(AnalyticsUtils.isParsable(Double.class, value)){
								jsonEntry.element(dimsArray[i],Double.parseDouble(value));
								continue;
							}
							
							jsonEntry.element(dimsArray[i],value);
						}
					}

					for (int i = 0; i < metricsArray.length; i++) {
						String value = entry.stringValueOf(metricsArray[i]);
						
						if(AnalyticsUtils.isParsable(Integer.class, value)){
							jsonEntry.element(metricsArray[i],Integer.parseInt(value));
							continue;
						}
						
						if(AnalyticsUtils.isParsable(Double.class, value)){
							jsonEntry.element(metricsArray[i],Double.parseDouble(value));
							continue;
						}
						
						jsonEntry.element(metricsArray[i],value);
					}
					jsonArray.add(jsonEntry);
				}
				jsonObject = new JSONObject();
				jsonObject.element("ids", tableId);
				jsonObject.element("startDate", startDate);
				jsonObject.element("endDate", endDate);
				jsonObject.element("feeds", jsonArray);
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new IGAnalyticsException("Malformed URL");
		} catch (IOException e) {
			e.printStackTrace();
			throw new IGAnalyticsException("IO exception");
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new IGAnalyticsException("Service exception");		
		}
		
		return jsonObject;
		
	}
	
}
