/**
 * 
 */
package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.lang.reflect.*;

import exceptions.IGAnalyticsException;

/**
 * Utils class for the analytics client.
 * 
 * @author Jhon Jaiver L&oacute;pez <jjlopezc@intergrupo.com>
 *
 */
public class AnalyticsUtils {

	/**
	 * Constants of the class
	 */
	private static final String GA_DATE_FORMAT = "yyyy-MM-dd";
	private static final String GA_PREFIX = "ga:";
	private static final int DIM_MAX_SIZE = 7; 
	private static final int MET_MAX_SIZE = 10; 
	
	/**
	 * Process the ids parameter for matching the analytics api 
	 * @param ids table id being processed
	 * @return table id with the 'ga:' prefix, if it doesn't have.
	 * 	null if ids is null or equals to ''
	 * 	ids otherwise
	 */
	public static String processIds (String ids){
		if(ids == null || ids.equals("")){
			return null;
		} else {
			if(ids.startsWith(GA_PREFIX)){
				return ids;
			} else {
				return GA_PREFIX+ids;
			}
		}
	}

	/**
	 * Validates and process the dimensions parameter. Can be supplied a 
	 * maximum of {@link utils.AnalyticsUtils#DIM_MAX_SIZE} dimensions.
	 * @param dimensions comma separated list of GA dimensions.
	 * @return comma separated list of GA dimensions (GA Prefix added if not present)
	 * @throws IGAnalyticsException thrown when validations are not passed.
	 */
	public static String processDimensions(String dimensions) throws IGAnalyticsException{
		if(dimensions == null || dimensions.equals("")){
			return null;
		}
		
		String[] dimensionsArray = dimensions.split(",");
		String result = "";
		
		if(dimensionsArray != null && dimensionsArray.length >DIM_MAX_SIZE){
			throw new IGAnalyticsException("Dimensions max size: "+DIM_MAX_SIZE);
		}
		
		if(dimensionsArray.length > 0){
			for (int i = 0; i < dimensionsArray.length; i++) {
				String dim = dimensionsArray[i];
				if(!dim.startsWith(GA_PREFIX)){
					result += GA_PREFIX+dimensionsArray[i];
				} else {
					result += dimensionsArray[i];			
				}
				if(dimensionsArray.length > (i+1)){
					result += ",";
				}
			}
		}
		
		return result;
	}
	

	/**
	 * Validates and process the metrics parameter. At least one metric value is required and 
	 * can be supplied a maximum of {@link utils.AnalyticsUtils#MET_MAX_SIZE} metrics.
	 * @param metrics comma separated list of GA metrics.
	 * @return comma separated list of GA metrics (GA Prefix added if not present)
	 * @throws IGAnalyticsException thrown when validations are not passed.
	 */
	public static String processMetrics(String metrics) throws IGAnalyticsException{
		if(metrics == null || metrics.equals("")){
			throw new IGAnalyticsException("At least one metric is required");
		}
		
		String[] metricsArray = metrics.split(",");
		String result = "";
		
		if(metricsArray != null && metricsArray.length >MET_MAX_SIZE){
			throw new IGAnalyticsException("Metrics max size: "+MET_MAX_SIZE);
		}
		
		if(metricsArray.length > 0){
			for (int i = 0; i < metricsArray.length; i++) {
				String met = metricsArray[i];
				if(!met.startsWith(GA_PREFIX)){
					result += GA_PREFIX+metricsArray[i];
				} else {
					result += metricsArray[i];			
				}
				if(metricsArray.length > (i+1)){
					result += ",";
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Returns the default date for the startDate vaule
	 * (31 days before current date)
	 * @return String parsed date in format GA_DATE_FORMAT
	 */
	public static String getDefaultStartDate() {
		DateFormat formatter = new SimpleDateFormat(GA_DATE_FORMAT);
		
		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.DATE,-31);
		return formatter.format(startDate.getTime());
	}
	
	/**
	 * Returns the default date for the endDate vaule
	 * (2 days before current date)
	 * @return String parsed date in format GA_DATE_FORMAT
	 */
	public static String getDefaultEndDate() {
		DateFormat formatter = new SimpleDateFormat(GA_DATE_FORMAT);
		
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.DATE,-2);
		return formatter.format(endDate.getTime());
	}

	/**
	 * Validates and process the sort parameter. If sort parameter is sent, 
	 * validate it's presence on the metrics and dimensions parameters
	 * @param sort comma separated list of GA metrics and/or dimensions to sort by
	 * @param metrics list of GA metrics sent by client
	 * @param dimensions  list of dimensions sent by client
	 * @return comma separated list of sort values. (adds the GA_PREFIX if not present)
	 * @throws IGAnalyticsException thrown if any value in sort is not present on 
	 * 	dimensions or metrics parameters
	 */
	public static String processSort(String sort, String dimensions, String metrics) throws IGAnalyticsException{
		if(sort == null || sort.equals("")){
			return null;
		}
		
		String sortResult = "";
		boolean reviewDimensions = dimensions != null;
		
		String[] sortArray = sort.replaceAll("-", "").split(",");
		for (int i = 0; i < sortArray.length; i++) {
			String currentSort = sortArray[i];
			if(reviewDimensions){
				if(!dimensions.contains(currentSort) && !metrics.contains(currentSort)){
					throw new IGAnalyticsException("Invalid sort value. Only values used in metrics or dimensions are allowed");
				}
			} else {
				if(!metrics.contains(currentSort)){
					throw new IGAnalyticsException("Invalid sort value. Only values used in metrics or dimensions are allowed");
				}
			}
			
			//Verify the GA_PREFIX
			if(!currentSort.startsWith(GA_PREFIX)){
				sortResult += GA_PREFIX+sortArray[i];
			} else {
				sortResult += sortArray[i];			
			}
			if(sortArray.length > (i+1)){
				sortResult += ",";
			}

		}
		
		return sortResult;
	}

	
	
	/**
	 * @author Jamie Bell
	 * 
	 * METHOD: isParsable<p><p>
	 * 
	 * This method will look through the methods of the specified <code>from</code> parameter
	 * looking for a public method name starting with "parse" which has only one String
	 * parameter.<p>
	 * 
	 * The <code>parser</code> parameter can be a class or an instantiated object, eg:
	 * <code>Integer.class</code> or <code>new Integer(1)</code>. If you use a
	 * <code>Class</code> type then only static methods are considered.<p>
	 * 
	 * When looping through potential methods, it first looks at the <code>Class</code> associated
	 * with the <code>parser</code> parameter, then looks through the methods of the parent's class
	 * followed by subsequent ancestors, using the first method that matches the criteria specified
	 * above.<p>
	 * 
	 * This method will hide any normal parse exceptions, but throws any exceptions due to
	 * programmatic errors, eg: NullPointerExceptions, etc. If you specify a <code>parser</code>
	 * parameter which has no matching parse methods, a NoSuchMethodException will be thrown
	 * embedded within a RuntimeException.<p><p>
	 * 
	 * Example:<br>
	 * <code>isParsable(Boolean.class, "true");<br>
	 * isParsable(Integer.class, "11");<br>
	 * isParsable(Double.class, "11.11");<br>
	 * Object dateFormater = new java.text.SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");<br>
	 * isParsable(dateFormater, "2001.07.04 AD at 12:08:56 PDT");<br></code>
	 * <p>
	 * 
	 * @param parser    The Class type or instantiated Object to find a parse method in.
	 * @param str   The String you want to parse
	 * 
	 * @return true if a parse method was found and completed without exception
	 * @throws java.lang.NoSuchMethodException If no such method is accessible 
	 */
	public static boolean isParsable(Object parser, String str) {
	    Class theClass = (parser instanceof Class? (Class)parser: parser.getClass());
	    boolean staticOnly = (parser == theClass), foundAtLeastOne = false;
	    Method[] methods = theClass.getMethods();

	    // Loop over methods
	    for (int index = 0; index < methods.length; index++) {
	        Method method = methods[index];

	        // If method starts with parse, is public and has one String parameter.
	        // If the parser parameter was a Class, then also ensure the method is static. 
	        if(method.getName().startsWith("parse") &&
	            (!staticOnly || Modifier.isStatic(method.getModifiers())) &&
	            Modifier.isPublic(method.getModifiers()) &&
	            method.getGenericParameterTypes().length == 1 &&
	            method.getGenericParameterTypes()[0] == String.class)
	        {
	            try {
	                foundAtLeastOne = true;
	                method.invoke(parser, str);
	                return true; // Successfully parsed without exception
	            } catch (Exception exception) {
	                // If invoke problem, try a different method
	                /*if(!(exception instanceof IllegalArgumentException) &&
	                   !(exception instanceof IllegalAccessException) &&
	                   !(exception instanceof InvocationTargetException))
	                        continue; // Look for other parse methods*/

	                // Parse method refuses to parse, look for another different method
	                continue; // Look for other parse methods
	            }
	        }
	    }

	    // No more accessible parse method could be found.
	    if(foundAtLeastOne) return false;
	    else throw new RuntimeException(new NoSuchMethodException());
	}


	/**
	 * METHOD: willParse<p><p>
	 * 
	 * A convienence method which calls the isParseable method, but does not throw any exceptions
	 * which could be thrown through programatic errors.<p>
	 * 
	 * Use of {@link #isParseable(Object, String) isParseable} is recommended for use so programatic
	 * errors can be caught in development, unless the value of the <code>parser</code> parameter is
	 * unpredictable, or normal programtic exceptions should be ignored.<p>
	 * 
	 * See {@link #isParseable(Object, String) isParseable} for full description of method
	 * usability.<p>
	 * 
	 * @param parser    The Class type or instantiated Object to find a parse method in.
	 * @param str   The String you want to parse
	 * 
	 * @return true if a parse method was found and completed without exception
	 * @see #isParseable(Object, String) for full description of method usability 
	 */
	public static boolean willParse(Object parser, String str) {
	    try {
	        return isParsable(parser, str);
	    } catch(Throwable exception) {
	        return false;
	    }
	}
	
	
}
