package exceptions;
/**
 * 
 */

/**
 * @author Jhon Jaiver L&oacute;pez <jjlopezc@intergrupo.com>
 *
 */
public class IGAnalyticsException extends Exception {

	private String message = null;

	public IGAnalyticsException() {
		super();
	}

	public IGAnalyticsException(String message) {
		super(message);
		this.message = message;
	}

	public IGAnalyticsException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
	
}
