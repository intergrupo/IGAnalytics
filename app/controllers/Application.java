package controllers;

import java.io.IOException;

import net.sf.json.JSONObject;
import play.mvc.Controller;
import utils.AnalyticsClient;
import utils.AnalyticsUtils;

import com.google.gdata.util.ServiceException;

import exceptions.IGAnalyticsException;

/**
 * Principal controller for processing the GAnalytics API requests.
 * 
 * @author Jhon Jaiver L&oacute;pez <jjlopezc@intergrupo.com>
 * 
 */
public class Application extends Controller {

    public static void index() {
        render();
    }

    /**
     * Fetches all the profiles available for the account
     */
    public static void getProfiles() {
        String username = params.get("user");
        String password = params.get("pass");

        JSONObject jsonResponse = null;

        if (username == null || password == null) {
            jsonResponse = new JSONObject();
            jsonResponse.accumulate("error", "Invalid data for login");
            renderJSON(jsonResponse);
        }

        AnalyticsClient ac = new AnalyticsClient(username, password);
        try {
            jsonResponse = new JSONObject();
            jsonResponse.accumulate("profiles", ac.getAvailableAccounts());
        } catch (ServiceException e) {
            jsonResponse.accumulate("error", "There was an error processing the request");
            renderJSON(jsonResponse);
            e.printStackTrace();
        } catch (IOException e) {
            jsonResponse.accumulate("error", "There was an error processing the request");
            renderJSON(jsonResponse);
            e.printStackTrace();
        }

        if (jsonResponse != null) {
            jsonResponse.accumulate("success", "true");
            renderJSON(jsonResponse);
        }

    }

    /**
     * Executes the query and returns the result as a JSON object.
     * 
     */
    public static void getFeeds() {
        String username = params.get("user");
        String password = params.get("pass");

        JSONObject jsonResponse = new JSONObject();

        if (username == null || password == null) {
            jsonResponse.accumulate("error", "Invalid data for login");
            renderJSON(jsonResponse);
        }

        AnalyticsClient ac = new AnalyticsClient(username, password);

        String ids = AnalyticsUtils.processIds(params.get("ids"));

        if (ids == null) {
            jsonResponse.accumulate("error", "Invalid data for query.");
            renderJSON(jsonResponse);
        }

        String dimensions = params.get("dimensions");
        try {
            dimensions = AnalyticsUtils.processDimensions(dimensions);
        } catch (IGAnalyticsException e) {
            e.printStackTrace();
            jsonResponse.accumulate("error", e.getMessage());
            renderJSON(jsonResponse);
        }

        String metrics = params.get("metrics");
        try {
            metrics = AnalyticsUtils.processMetrics(metrics);
        } catch (IGAnalyticsException e) {
            e.printStackTrace();
            jsonResponse.accumulate("error", e.getMessage());
            renderJSON(jsonResponse);
        }

        String startDate = params.get("start-date");
        if (startDate == null || startDate.equals("")) {
            startDate = AnalyticsUtils.getDefaultStartDate();
        }

        String endDate = params.get("end-date");
        if (endDate == null || endDate.equals("")) {
            endDate = AnalyticsUtils.getDefaultEndDate();
        }

        String maxResults = params.get("max-results");
        Integer maxResultsNumber = maxResults != null && !maxResults.equals("") ? Integer.parseInt(maxResults) : 50;

        String sort = params.get("sort");
        if (sort != null && !sort.equals("")) {
            try {
                sort = AnalyticsUtils.processSort(sort, dimensions, metrics);
            } catch (IGAnalyticsException e) {
                e.printStackTrace();
                jsonResponse.accumulate("error", e.getMessage());
                renderJSON(jsonResponse);
            }
        }

        String filters = params.get("filters");

        try {
            jsonResponse = ac.getDataFeeds(ids, dimensions, metrics, startDate, endDate, maxResultsNumber, sort, filters);
        } catch (IGAnalyticsException e) {
            jsonResponse.accumulate("error", e.getMessage());
            renderJSON(jsonResponse);
            e.printStackTrace();
        }

        if (jsonResponse != null) {
            jsonResponse.accumulate("success", "true");
            renderJSON(jsonResponse);
        }

    }

}